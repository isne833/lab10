#include <iostream>
#include "list.h"
using namespace std;

List::~List() 
{
	for (Node* p; !isEmpty(); ) 
	{
		p = head->next;
		delete head;
		head = p;
	}
}

void List::display()
{
	Node* temp = new Node();
	temp = head;
	while (temp != NULL)
	{
		cout << temp->info << " ";
		temp = temp->next;
	}
}

void List::headPush(int n)
{
	Node* temp = new Node();	
	temp->info = n;		
	temp->prev = NULL;		
	temp->next = head;		

	
	if (isEmpty())
	{
		tail = temp;
	}

	
	else
	{
		head->prev = temp;
	}
	head = temp;	
}

void List::tailPush(int n)
{
	Node* temp = new Node();	
	temp->info = n;		
	temp->prev = tail;		
	temp->next = NULL;		

	
	if (isEmpty())
	{
		head = temp;
	}

	
	else
	{
		tail->next = temp;
	}
	tail = temp;	
}

int List::headPop()
{
	int n = head->info;		
	if (head != NULL)
	{
		Node* temp = head;		
		head = head->next;		
		delete temp;	
		if (head != NULL)
		{
			head->prev = NULL;		
		}
	}
	return n;	
}

int List::tailPop()
{
	int n = tail->info;		
	if (tail != NULL)
	{
		Node* temp = tail;		
		tail = temp->prev;		
		delete temp;	
		if (tail != NULL)
		{
			tail->next = NULL;		
		}
	}
	return n;	
}

void List::deleteNode(int n)
{

	if (isEmpty())
	{
		return;
	}

	Node* temp = head;

	while (temp->info != n)
	{
		temp = temp->next;

		
		if (temp->next == NULL)
		{
			cout << "invalid | ";
			return;
		}
	}
	
	
	if (head->info == n)
	{
		headPop();
	}

	
	else if (tail->info == n)
	{
		tailPop();
	}

	else if (temp != NULL)
	{
		temp->prev->next = temp->next;	
		temp->next->prev = temp->prev;	
		delete temp;	
	}
	else
	{
		cout << "invalid";
	}
}

bool List::isInList(int n)
{
	
	if (isEmpty())
	{
		return false;
	}

	Node* temp = head;		

	while (temp->info != n)
	{
		
		if (temp->next == NULL)
		{
			cout << "This list doesn't has this Node\n";
			return false;
		}
		temp = temp->next;
	}

	if (temp != NULL)
	{
		cout << "This list has this Node\n";
		return true;
	}
}
