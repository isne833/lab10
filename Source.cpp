#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	List a2;

	//head push
	a2.headPush(3); a2.headPush(2); a2.headPush(1);
	cout << "headPush : ";	a2.display(); 
	cout << endl;

	//tail push
	a2.tailPush(4); a2.tailPush(5); a2.tailPush(6);
	cout << "tailPush : "; a2.display(); 
	cout << endl;

	//head pop
	cout << "headPop(" << a2.headPop() << ") : "; a2.display(); 
	cout << endl;

	//tail pop
	cout << "tailPop(" << a2.tailPop() << ") : "; a2.display(); 
	cout << endl;

	//delete Node
	cout << "deleteNode(4) : "; a2.deleteNode(4); a2.display(); 
	cout << endl;
	
	cout << "deleteNode(999) : "; a2.deleteNode(999); a2.display(); 
	cout << endl;

	//find Node
	cout << "isInList(3) : "; a2.isInList(3);
	cout << "isInList(999) : "; a2.isInList(999);
}
